# Featury
* Aplikacja odpytuje serwer o status połączenia.
* Możliwość automatycznego wywołania dzwonienia po linku /ringing/(numer)
* Wykorzystanie routera.
* Świąteczny wystrój.
* Odliczanie czasu oparte na setInterval.

# Uruchomienie
* Backend: node server.js
* Frontend: ng serve

# Angular-Cli
This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.5.5.
