import { Component, OnInit } from '@angular/core';
import { CallService } from "../call.service";
import { Router } from '@angular/router';
import { ngForm } from '@angular/forms';

@Component({
  selector: 'app-widget',
  templateUrl: './widget.component.html',
  styleUrls: ['./widget.component.css']
})
export class WidgetComponent implements OnInit {
  numer: string;
  validator = /^[0-9]{9}$/

  constructor(private router: Router) { }
  call() {
    if (this.isValidNumber()) {
      // this.callService.placeCall(this.numer);
    }
  }
  isValidNumber() {
    return this.validator.test(this.numer);
  }
  ngOnInit() {
  }
  onSubmit() {
    this.router.navigate(['/ringing/' + this.numer]);
  }

}
